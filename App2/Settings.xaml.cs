﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace App2
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class BlankPage1 : Page
    {
        //******************
        // Member
        //******************
        bool AppModusDunkelOderHellZuruckandern = false;

        public BlankPage1()
        {
            this.InitializeComponent();

            if ((int)Application.Current.RequestedTheme == 0)
            {
                DarkModeAktivieren.IsOn = false;
            }
            else
            {
                DarkModeAktivieren.IsOn = true;
            }
            DarkModeAktivieren.Toggled += DarkModeAktivieren_Toggled;


            // Zurück button einblenden
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            SystemNavigationManager.GetForCurrentView().BackRequested += (s, e) =>
            {
                // TODO: Go back to the previous page
            };
        }

        private async void DarkModeAktivieren_Toggled(object sender, RoutedEventArgs e)
        {
            ApplicationDataContainer AppEinstellungen = ApplicationData.Current.LocalSettings;

            if (DarkModeAktivieren.IsOn)
            {
                AppEinstellungen.Values["AppModusDunkelOderHell"] = 1;
            }
            else
            {
                AppEinstellungen.Values["AppModusDunkelOderHell"] = 0;
            }

            if (AppModusDunkelOderHellZuruckandern == false)
            {
                AppModusDunkelOderHellZuruckandern = true;
                MessageDialog dialog = new MessageDialog("Die App muss neu gestartet werden, um die Einstellung zu übernehmen.\n" +
                                                         "Bitte starten sie die App neu!" +
                                                         "\nSoll die Anwendung beendet werden?", "Neustart erforderlich");
                dialog.Commands.Add(new UICommand("Beenden") { Id = 0 });
                dialog.Commands.Add(new UICommand("Später") { Id = 1 });

                dialog.DefaultCommandIndex = 0;
                dialog.CancelCommandIndex = 1;
                var result = await dialog.ShowAsync();

                if ((int)result.Id == 0)
                {
                    // Neustart
                    App.Current.Exit();
                }
            }
            else
            {
                AppModusDunkelOderHellZuruckandern = false;
            }
        }
    }
}
