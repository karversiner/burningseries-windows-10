﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Vorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409 dokumentiert.

namespace App2
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        // ****************
        // Member
        // ****************
        string[] m_ComboBoxAuswahlSerienFavEtc = {"Alle Serien", "Favoriten" };
        ObservableCollection<MyUserControl1> AlleSerienListe = new ObservableCollection<MyUserControl1>();

        public MainPage()
        {
            this.InitializeComponent();
            // Deaktivert den Frame Counter während des Debuggens
            Application.Current.DebugSettings.EnableFrameRateCounter = false;
            // Zwischenspeichern der Seiten, damit beim zurückkehren die alte Seite nicht neu geladen wird
            this.NavigationCacheMode = NavigationCacheMode.Enabled;

            // Mögliche Auswahlen anzeigen (Favoriten, Alle Serien etc...)
            foreach (string item in m_ComboBoxAuswahlSerienFavEtc)
            {
                ComboBoxAuswahlSerienFavETC.Items.Add(item);
            }
            // Das erste Element in der Liste auswählen (alle Serien)
            ComboBoxAuswahlSerienFavETC.SelectedIndex = 0;
        }

        private void AppBarFlyoutAktualisieren_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(StreamVideo));
            //TODO: Serien liste neu Laden / Aktualisieren
        }

        /// <summary>
        /// Weiterleitung zu den Einstellungen
        /// </summary>
        private void AppBarFlyoutEinstellungen_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(BlankPage1));
        }

        private void AppBarFlyoutUeber_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(UeberMich));
        }

        private void BtnSuche_Click(object sender, RoutedEventArgs e)
        {
            BtnSuche.Visibility = Visibility.Collapsed;
            ComboBoxAuswahlSerienFavETC.Visibility = Visibility.Collapsed;
            TextBoxSuche.Visibility = Visibility.Visible;
            TextBoxSuche.Focus(FocusState.Pointer);
        }

        private void TextBoxSuche_LostFocus(object sender, RoutedEventArgs e)
        {
            BtnSuche.Visibility = Visibility.Visible;
            ComboBoxAuswahlSerienFavETC.Visibility = Visibility.Visible;
            TextBoxSuche.Visibility = Visibility.Collapsed;
            TextBoxSuche.PlaceholderText = "Suche...";
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // *************************
            // Lade Animation anzeigen
            // *************************
            StackPanel DatenWerdenGeladenAnimationPanelGruppe = new StackPanel();
            DatenWerdenGeladenAnimationPanelGruppe.HorizontalAlignment = HorizontalAlignment.Stretch;

            ProgressBar ProgressBarLadeAnimationWahrenDatenVomServerGeholtWerden = new ProgressBar();
            ProgressBarLadeAnimationWahrenDatenVomServerGeholtWerden.IsIndeterminate = true;
            ProgressBarLadeAnimationWahrenDatenVomServerGeholtWerden.Margin = new Thickness(5);

            TextBlock TextBlockUserInfoDasDatenGeladenWerden = new TextBlock();
            TextBlockUserInfoDasDatenGeladenWerden.Text = "Warte auf BuringSeries";
            TextBlockUserInfoDasDatenGeladenWerden.HorizontalAlignment = HorizontalAlignment.Center;
            TextBlockUserInfoDasDatenGeladenWerden.Margin = new Thickness(5);
            TextBlockUserInfoDasDatenGeladenWerden.TextWrapping = TextWrapping.Wrap;

            DatenWerdenGeladenAnimationPanelGruppe.Children.Add(TextBlockUserInfoDasDatenGeladenWerden);
            DatenWerdenGeladenAnimationPanelGruppe.Children.Add(ProgressBarLadeAnimationWahrenDatenVomServerGeholtWerden);
            SteuerelementeHauptfenser.Children.Add(DatenWerdenGeladenAnimationPanelGruppe);
            // *************************
            // *************************

            var task = Task.Run(async () =>
            {
                Klassen.BurningSeriesAPI.CBurningSeries BurningSeriesAPI = new Klassen.BurningSeriesAPI.CBurningSeries();
                string BSQuellcode = BurningSeriesAPI.GetBurningSeriesQuellcode().Result;

                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                 {
                     if (BSQuellcode.Contains("Webseite nicht erreichbar. Fehler:"))
                     {
                     // Fehlermeldung
                     TextBlockUserInfoDasDatenGeladenWerden.Text = BSQuellcode;
                         DatenWerdenGeladenAnimationPanelGruppe.Children.Remove(ProgressBarLadeAnimationWahrenDatenVomServerGeholtWerden);
                         return;
                     }
                     else
                     {
                         TextBlockUserInfoDasDatenGeladenWerden.Text = "Verarbeite Daten";
                     }
                 });

                List<string>[] AlleSerien = BurningSeriesAPI.GetAlleSerien(BSQuellcode);
                List<string> AlleSerienLink = AlleSerien[0];
                List<string> AlleSerienTitel = AlleSerien[1];

                try
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                    {
                        for (int i = 0; i < 100; i++)
                        {
                            AlleSerienListe.Add(new MyUserControl1 { Titel = AlleSerienTitel[i], Link = AlleSerienLink[i], HorizontalAlignment = HorizontalAlignment.Stretch });
                        }
                        AlleSerien = null;
                        AlleSerienLink = null;
                        AlleSerienTitel = null;

                        this.DataContext = AlleSerienListe;
                    });

                }
                catch (Exception ex)
                {

                }

                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        SteuerelementeHauptfenser.Children.Remove(DatenWerdenGeladenAnimationPanelGruppe);
                    });
            });
        }
    }
}
