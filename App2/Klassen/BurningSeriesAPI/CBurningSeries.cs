﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2.Klassen.BurningSeriesAPI
{
    public class CBurningSeries
    {
        // *********************
        // Member
        // *********************
        private static string BsToUrlWebseite = @"https://bs.to/";
        private static string BsToUrlAlleSerien = @"serie-alphabet";


        public async Task<List<string>[]> Start()
        {
            // Quelltext der Seite bs.to anfordern
            string Quellcode = await CNetzwerk.getWebseitQuellCode(BsToUrlWebseite + BsToUrlAlleSerien);

            // Quellcode bearbeiten
            QuellCodeBearbeiten QuellCodeBearbeiten = new QuellCodeBearbeiten();
            return QuellCodeBearbeiten.GetSerienElemente(Quellcode);
        }

        public async Task<string> GetBurningSeriesQuellcode()
        {
            // Quelltext der Seite bs.to anfordern
            return await CNetzwerk.getWebseitQuellCode(BsToUrlWebseite + BsToUrlAlleSerien);
        }

        public List<string>[] GetAlleSerien(string BurningSeriesQuellcode)
        {
            QuellCodeBearbeiten QuellCodeBearbeiten = new QuellCodeBearbeiten();
            return QuellCodeBearbeiten.GetSerienElemente(BurningSeriesQuellcode);
        }
    }
}
