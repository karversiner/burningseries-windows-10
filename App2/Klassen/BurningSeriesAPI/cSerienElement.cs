﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2.Klassen.BurningSeriesAPI
{
    public class CSerienElement
    {
        //*****************
        //Member
        //*****************

        private string m_Titel;
        private string m_Untertitel;
        private string m_Url;

        public string Titel
        {
            get { return m_Titel; }
            set
            {
                m_Titel = value;
            }
        }

        public string Untertitel
        {
            get { return m_Untertitel; }
            set
            {
                m_Untertitel = value;
            }
        }

        public string Url
        {
            get { return m_Url; }
            set
            {
                m_Url = value;
            }
        }
    }
}
