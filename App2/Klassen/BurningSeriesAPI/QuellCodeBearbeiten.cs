﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2.Klassen.BurningSeriesAPI
{
    public class QuellCodeBearbeiten
    {
        //
        // Member
        //
        // So können wir die Position bestimmen am wann die "Serien-Elemente" anfangen
        string m_AnfangSeite = "<div class=\"genre\">";
        string m_AnfangEndeSeite = "<li><a href=\"";

        // Serien Element ermitteln - Hieraus können wir Name und Link ermitteln
        string m_SeElAnfangLink = "<a href=\"";
        string m_SeElEndeLink = "\" ";
        string m_SeElAnfangTitel = "\">";
        string m_SeElEndeTitel = "</a></li>";

        // Serien-Elemente ENDE - Hier enden die Serien Elemente
        string m_EndeSeElEnde = "timeout = null";

        public List<string>[] GetSerienElemente(string Quellcode)
        {
            int Anfang = 0;
            int Ende = 0;

            List<string> SerienLink = new List<string>();
            List<string> SerienTitel = new List<string>();

            // Anfang der "Serien-Elemente" bestimmen
            Anfang = Quellcode.IndexOf(m_AnfangSeite);
            Quellcode = Quellcode.Substring(Anfang);

            Ende = Quellcode.IndexOf(m_AnfangEndeSeite);
            Quellcode = Quellcode.Substring(Ende);

            // Ende der "Serien-Elemente" bestimmen
            Ende = Quellcode.LastIndexOf(m_EndeSeElEnde);
            Quellcode = Quellcode.Substring(0, Ende);

            while (true)
            {
                
                try
                {
                    // Link hinzufügen
                    Anfang = Quellcode.IndexOf(m_SeElAnfangLink) + m_SeElAnfangLink.Length;
                    Ende = Quellcode.IndexOf(m_SeElEndeLink);

                    if (Ende == -1) { break; }

                    SerienLink.Add(Quellcode.Substring(Anfang, Ende - Anfang));

                    // Titel hinzufügen
                    Anfang = Quellcode.IndexOf(m_SeElAnfangTitel) + m_SeElAnfangTitel.Length;
                    Ende = Quellcode.IndexOf(m_SeElEndeTitel);
                    SerienTitel.Add(Quellcode.Substring(Anfang, Ende - Anfang));

                    Quellcode = Quellcode.Substring(Ende + m_SeElAnfangLink.Length);
                }
                catch (Exception ex)
                {
                    // TODO Fehlermeldung
                }
            }

            List<string>[] AlleSerien = new List<string>[] { SerienLink, SerienTitel };

            return AlleSerien;
        }
    }
}
