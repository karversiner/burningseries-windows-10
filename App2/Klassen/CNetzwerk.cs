﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace App2.Klassen
{
    public class CNetzwerk
    {
        public static async Task<string> getWebseitQuellCode(string url)
        {
            try
            {
                HttpClient client = new HttpClient();
                return await client.GetStringAsync(url);
            }
            catch (Exception ex)
            {
                switch (ex.Message)
                {
                    case "Response status code does not indicate success: 404 (Not Found).": return "Webseite nicht erreichbar. Fehler:\n" + ex.Message;
                    default:
                        return ex.Message;
                }
            }
        }
    }
}
