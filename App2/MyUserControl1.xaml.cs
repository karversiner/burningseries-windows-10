﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace App2
{
    public sealed partial class MyUserControl1 : UserControl, INotifyPropertyChanged
    {
        //*****************
        // Private Member
        //*****************
        private string m_Titel;
        private string m_Untertitel;
        private string m_Link;
        private string m_Bild;

        public event PropertyChangedEventHandler PropertyChanged;

        //*****************
        // Get / SET
        //*****************
        public string Titel
        {
            get { return m_Titel; }
            set
            {
                m_Titel = value;
                InfoEigenschaftAnderung("Titel");
            }
        }

        public string Link
        {
            get { return m_Link; }
            set
            {
                m_Link = value;
                InfoEigenschaftAnderung("Link");
            }
        }

        public string Bild
        {
            get { return m_Bild; }
            set
            {
                m_Bild = value;
                InfoEigenschaftAnderung("Bild");
            }
        }

        public MyUserControl1()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //BitmapImage bitmapImage = new BitmapImage(new Uri(this.BaseUri, "/Assets/Stern/SternBlue64.png"));
            //ImageFavorit.Source = bitmapImage;

            //+ # - TODO + # -
            // Zu Favoriten hinzufügen
        }

        private void InfoEigenschaftAnderung(string v)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(v));
            }
        }
    }
}
